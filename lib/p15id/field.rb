module P15ID
  class Field
    attr_reader :field, :value

    SEP = "\x16"
    FIELD_END = "\x08"
    MEASURE_LIST = %w{ Val Low High Unit Flag EditFlag HighLowFlag }
    PATINFO_LIST = %w{ SampleID Name Gender AgeType AgeVal ChargeType
    SamSourc ChartNo BedNo InsNo Dept Sender Tester Checker Remark }
    LIST = MEASURE_LIST + PATINFO_LIST

    #Val  Parameter value indicated by SD field
    #Low  Lower limit of analysis result
    #High Upper limit of analysis result
    #Unit Unit of parameter
    #Flag Suspect sign for parameters

    def initialize(field:, value: "")
      raise "Invalid field '#{field}'" unless LIST.include?(field)
      @field = field
      @value = value
    end

    def to_s(sep: ",", field_end: ";")
      "#{@field}#{sep}#{@value}#{field_end}"
    end

    def to_bytes
      to_s(sep: SEP, field_end: FIELD_END)
    end
  end
end
