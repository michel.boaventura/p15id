module P15ID
  class Segment
    attr_reader :segment, :fields

    MEASURE = %w{ HCT HGB Lymph# Lymph% MCH MCHC MCV Mid# Mid% MPV Neu#
    Neu% PCT PDW P-LCR PLT RBC RDW-CV RDW-SD WBC }
    PATINFO = %w{ PatInfo }
    LIST = MEASURE + PATINFO

    SEGMENT_END = "\x04"
    SD_FD_SEP = "\x0C"

    def initialize(segment:)
      raise "Invalid Segment '#{segment}'" unless LIST.include?(segment)

      @segment = segment
      @fields = []
    end

    def add_field(field)
      return if @fields.any? { |f| f.field == field.field }

      if (MEASURE.include?(@segment) && !Field::MEASURE_LIST.include?(field.field)) ||
          (PATINFO.include?(@segment) && !Field::PATINFO_LIST.include?(field.field))
        raise("Field '#{field.field}' not compatible with Segment '#{@segment}'")
      end

      @fields << field
    end

    def to_s(fields: nil, segment_end: "$", sd_fd_sep: ":")
      fields ||= @fields.map(&:to_s).join
      "#{@segment}#{sd_fd_sep}#{fields}#{segment_end}"
    end

    def to_bytes
      fields = @fields.map(&:to_bytes).join
      to_s(fields: fields, segment_end: SEGMENT_END, sd_fd_sep: SD_FD_SEP)
    end
  end
end
