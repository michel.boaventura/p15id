module P15ID
  class Message
    attr_reader :segments

    MESSAGE_START = "\x05"
    MESSAGE_END = "\x0a"
    MESSAGE_DESCRIPTION = "CTR"
    MD_SD_SEPARATOR = "\x03"

    def initialize
      @segments = []
    end

    def add_segment(segment)
      return if @segments.any? { |s| s.segment == segment.segment }

      @segments << segment
    end

    def to_s(segments: nil, message_start: "{", message_end: "}", md_sd_separator: "#")
      segments ||= @segments.map(&:to_s).join
      "#{message_start}#{MESSAGE_DESCRIPTION}#{md_sd_separator}#{segments}#{message_end}"
    end

    def to_bytes
      segments = @segments.map(&:to_bytes).join
      to_s(segments: segments, message_start: MESSAGE_START, message_end: MESSAGE_END, md_sd_separator: MD_SD_SEPARATOR)
    end

  end
end
