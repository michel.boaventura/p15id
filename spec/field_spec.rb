require 'p15id'

include P15ID

describe Field do
  context "Misc" do
    it "doesn't initialize without arguments" do
      expect{Field.new}.to raise_error(ArgumentError)
    end
    it "uses the right separator" do
      expect(Field::SEP).to eq("\x16")
    end
    it "uses the right field end" do
      expect(Field::FIELD_END).to eq("\x08")
    end
  end

  context "Field descriptions and values" do
    it "accepts valid field" do
      expect{Field.new(field: "Val")}.not_to raise_error
    end
    it "throws exception with invalid field" do
      expect{Field.new(field: "F")}.to raise_error("Invalid field 'F'")
    end
    it "doesn't requid a field's value" do
      expect{Field.new(field: "Val")}.not_to raise_error
    end
  end

  context "Exporting fields" do
    subject { Field.new(field: "Val", value: "42.0") }

    it "implements #to_s" do
      expect(subject.to_s).to eq("Val,42.0;")
    end

    it "implements #to_bytes" do
      expect(subject.to_bytes).to eq("Val\x1642.0\x08")
    end
  end

end
