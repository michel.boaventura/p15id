require 'p15id'

include P15ID

describe Message do
  context "Misc" do
    it "uses the right message start" do
      expect(Message::MESSAGE_START).to eq("\x05")
    end
    it "uses the right message end" do
      expect(Message::MESSAGE_END).to eq("\x0a")
    end
    it "uses the right message description" do
      expect(Message::MESSAGE_DESCRIPTION).to eq("CTR")
    end
    it "uses the right MD-SD separator" do
      expect(Message::MD_SD_SEPARATOR).to eq("\x03")
    end
  end

  context "Segments" do
    it "accepts segments with #add_segments" do
      msg = Message.new
      segment = Segment.new(segment: "HCT")
      msg.add_segment segment

      expect(msg.segments).to eq([segment])
    end
    it "keeps the first segment if two of the same type are added" do
      seg1 = Segment.new(segment: "HCT")
      seg2 = Segment.new(segment: "HCT")
      msg = Message.new

      msg.add_segment seg1
      msg.add_segment seg2

      expect(msg.segments).to eq([seg1])
    end
  end

  context "Exporting message" do
    subject {
      msg = Message.new
      seg1 = Segment.new(segment: "HCT")
      seg2 = Segment.new(segment: "HGB")

      seg1.add_field Field.new(field: "Val", value: "41.0")
      seg2.add_field Field.new(field: "Low", value: "22.0")

      msg.add_segment seg1
      msg.add_segment seg2

      msg
    }

    it "implements #to_s" do
      expect(subject.to_s).to eq("{CTR#HCT:Val,41.0;$HGB:Low,22.0;$}")
    end
    it "implements #to_bytes" do
      expect(subject.to_bytes).to eq("\x05CTR\x03HCT\x0cVal\x1641.0\x08\x04HGB\x0cLow\x1622.0\x08\x04\x0a")
    end
  end
end
