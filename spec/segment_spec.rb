require 'p15id'

include P15ID

describe Segment do
  context "Misc" do
    it "doesn't initialize without arguments" do
      expect{Segment.new}.to raise_error(ArgumentError)
    end
    it "uses the right segment end" do
      expect(Segment::SEGMENT_END).to eq("\x04")
    end
    it "uses the right SD-FD separator" do
      expect(Segment::SD_FD_SEP).to eq("\x0C")
    end
  end

  context "Segment descriptions" do
    it "accepts valid segment" do
      expect{Segment.new(segment: "HCT")}.not_to raise_error
    end

    it "throws exception with invalid segment" do
      expect{Segment.new(segment: "F")}.to raise_error("Invalid Segment 'F'")
    end
  end

  context "Fields" do
    it "accepts fields with #add_field" do
      field = Field.new(field: "Val", value: "42.0")
      seg = Segment.new(segment: "HCT")
      seg.add_field(field)

      expect(seg.fields).to eq([field])
    end

    it "keeps the first field if two of the same type are added" do
      field1 = Field.new(field: "Val", value: "41.0")
      field2 = Field.new(field: "Val", value: "42.0")

      seg = Segment.new(segment: "HCT")
      seg.add_field(field1)
      seg.add_field(field2)

      expect(seg.fields).to eq([field1])
    end

    it "only accept fields which are valid for the segment" do
      seg = Segment.new(segment: "HCT")
      f1 = Field.new(field: "SampleID")

      expect {seg.add_field f1}.to raise_error(
        "Field 'SampleID' not compatible with Segment 'HCT'"
      )

      seg = Segment.new(segment: "PatInfo")
      f1 = Field.new(field: "Val")

      expect {seg.add_field f1}.to raise_error(
        "Field 'Val' not compatible with Segment 'PatInfo'"
      )

    end
  end

  context "Exporting segment" do
    subject {
      seg = Segment.new(segment: "HCT")
      seg.add_field Field.new(field: "Val", value: "41.0")
      seg.add_field Field.new(field: "Low", value: "22.0")
      seg
    }

    it "implements #to_s" do
      expect(subject.to_s).to eq("HCT:Val,41.0;Low,22.0;$")
    end

    it "implements #to_bytes" do
      expect(subject.to_bytes).to eq("HCT\x0CVal\x1641.0\x08Low\x1622.0\x08\x04")
    end
  end
end
