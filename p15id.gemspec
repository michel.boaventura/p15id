# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'p15id/version'

Gem::Specification.new do |spec|
  spec.name          = "p15id"
  spec.version       = P15ID::VERSION
  spec.authors       = ["Michel Boaventura"]
  spec.email         = ["michel.boaventura@gmail.com"]
  spec.summary       = "15ID protocol implementation"
  spec.description   = "A Ruby's implementation of the 15ID protocol"
  spec.homepage      = "https://github.com/michelboaventura/p15id"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.3"
  spec.add_development_dependency "guard-rspec", "~> 4.6"
end
